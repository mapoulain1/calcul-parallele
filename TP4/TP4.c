/**
 * @author Maxime POULAIN
 * @date 15/03/2023
 * @file TP4.c
 * 
 * Exemple de sortie : 
 * 1)
    mpirun -np 8 ./out 32
	Tableau :
	0 2 11 11 12 13 18 21 23 24 33 37 39 41 42 45 47 51 52 56 56 59 65 73 76 77 77 86 91 94 99 99

   2)
	mpirun -np 8 ./out 20
    Erreur 20 n'est pas divisible par 8

   3)
    mpirun -np 8 ./out 8
	Tableau :
	2 12 52 56 59 77 91 99 
 */


#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
	@brief Affiche l'utilisation de la commande dans le terminal.
*/
void usage();

/**
	@brief Génère un tableau de nombres aléatoires entre 0 et 99.
	@param local_a Tableau de nombres générés.
	@param local_n Taille du tableau local_a.
	@param my_rank Identifiant du processus appelant.
*/
void generate_tableau(int local_a[], const int local_n, const int my_rank);

/**
	@brief Récupère les arguments donnés dans le terminal.
	@param argc Nombre d'arguments dans le terminal.
	@param argv Tableau d'arguments du terminal.
	@param global_np Pointeur vers le nombre d'éléments total dans le tableau.
	@param local_np Pointeur vers le nombre d'éléments par processus.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void get_args(const int argc, char *argv[], int *global_np, int *local_np, const int my_rank, const int p);

/**
	@brief Affiche le contenu du tableau pour chaque processus.
	@param local_a Tableau de nombres trié pour chaque processus.
	@param local_n Taille du tableau local_a.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void print_tableau(int local_a[], const int local_n, const int my_rank, const int p);

/**
	@brief Compare deux éléments d'un tableau.
	@param a_p Pointeur vers le premier élément.
	@param b_p Pointeur vers le deuxième élément.
	@return -1 si a est plus petit que b, 0 si a et b sont égaux, 1 si a est plus grand que b.
*/
int compare(const void *a_p, const void *b_p);

/**
	@brief Trie le tableau local_a pour chaque processus.
	@param local_a Tableau de nombres générés pour chaque processus.
	@param local_n Taille du tableau local_a.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void sort(int local_a[], const int local_n, const int my_rank, const int p);

/**
	@brief Réalise une itération de l'algo de tri pair impair.
	@param local_a Tableau de nombres trié pour chaque processus.
	@param temp_b Tableau temporaire pour stocker les éléments pairs.
	@param temp_c Tableau temporaire pour stocker les éléments impairs.
	@param local_n Taille du tableau local_a.
	@param phase Phase de l'itération.
	@param even_partner Identifiant du processus partenaire pair.
	@param odd_partner Identifiant du processus partenaire impair.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void odd_even_iter(int local_a[], int temp_b[], int temp_c[], const int local_n, const int phase, const int even_partner, const int odd_partner, const int my_rank, int p);

/**
	@brief Fusionne deux tableaux triés en ordre croissant pour obtenir un seul tableau trié en ordre croissant.
	La fonction merge_split_low fusionne deux tableaux triés en ordre croissant, local_a et temp_b, en un seul tableau trié en ordre croissant temp_c.
	Les éléments de local_a sont comparés avec ceux de temp_b et le plus petit élément est placé dans temp_c.
	Cette opération est répétée jusqu'à ce que tous les éléments soient placés dans temp_c.
	@param local_a Tableau d'entiers contenant les éléments de la première moitié du tableau à fusionner.
	@param temp_b Tableau d'entiers contenant les éléments de la deuxième moitié du tableau à fusionner.
	@param temp_c Tableau d'entiers où les éléments triés sont stockés.
	@param local_n Nombre d'éléments dans le tableau local_a.
*/
void merge_split_low(int local_a[], int temp_b[], int temp_c[], const int local_n);

/**
	@brief Fusionne deux tableaux triés en ordre décroissant pour obtenir un seul tableau trié en ordre décroissant.
	La fonction merge_split_high fusionne deux tableaux triés en ordre décroissant, local_a et temp_b, en un seul tableau trié en ordre décroissant temp_c.
	Les éléments de local_a sont comparés avec ceux de temp_b et le plus grand élément est placé dans temp_c.
	Cette opération est répétée jusqu'à ce que tous les éléments soient placés dans temp_c.
	@param local_a Tableau d'entiers contenant les éléments de la première moitié du tableau à fusionner.
	@param temp_b Tableau d'entiers contenant les éléments de la deuxième moitié du tableau à fusionner.
	@param temp_c Tableau d'entiers où les éléments triés sont stockés.
	@param local_n Nombre d'éléments dans le tableau local_a.
*/
void merge_split_high(int local_a[], int temp_b[], int temp_c[], const int local_n);

/**
	@brief Fonction principale qui initialise MPI, récupère les arguments, génère le tableau, le trie, affiche le résultat, puis libère la mémoire allouée et finalise MPI.
*/
int main(int argc, char *argv[]) {
	int my_rank, p;
	int global_n;
	int local_n;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	get_args(argc, argv, &global_n, &local_n, my_rank, p);
	int *local_a = malloc(local_n * sizeof(int));

	generate_tableau(local_a, local_n, my_rank);

	sort(local_a, local_n, my_rank, p);

	print_tableau(local_a, local_n, my_rank, p);

	free(local_a);
	MPI_Finalize();

	return 0;
}

/**
	@brief Affiche l'utilisation de la commande dans le terminal.
*/
void usage() {
	printf("usage:  mpi_run -np <p> ./out <n>\n");
	printf("   - p: le nombre de processus à lancer \n");
	printf("   - n: le nombre d'éléments à générer dans le tableau\n");
	printf("        n doit être divisable par le nombre de processus\n");
}


/**
	@brief Génère un tableau de nombres aléatoires entre 0 et 99 pour chaque processus.
	@param local_a Tableau de nombres générés pour chaque processus.
	@param local_n Taille du tableau local_a.
	@param my_rank Identifiant du processus appelant.
*/
void generate_tableau(int local_a[], const int local_n, const int my_rank) {
	srand(my_rank + 12345);
	for (int i = 0; i < local_n; i++)
		local_a[i] = rand() % 100;
}

/**
	@brief Récupère les arguments donnés dans le terminal.
	@param argc Nombre d'arguments dans le terminal.
	@param argv Tableau d'arguments du terminal.
	@param global_np Pointeur vers le nombre d'éléments total dans le tableau.
	@param local_np Pointeur vers le nombre d'éléments par processus.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void get_args(const int argc, char *argv[], int *global_np, int *local_np, const int my_rank, const int p) {
	if (my_rank == 0) {
		if (argc != 2) {
			usage();
			*global_np = -1;
		} else {
			*global_np = strtol(argv[1], NULL, 10);
			if (*global_np % p != 0) {
				printf("Erreur %d n'est pas divisible par %d\n", *global_np, p);
				usage();
				*global_np = -1;
			}
		}
	}

	MPI_Bcast(global_np, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if (*global_np <= 0) {
		MPI_Finalize();
		exit(-1);
	}

	*local_np = *global_np / p;
}

/**
	@brief Affiche le contenu du tableau pour chaque processus.
	@param local_a Tableau de nombres trié pour chaque processus.
	@param local_n Taille du tableau local_a.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void print_tableau(int local_a[], const int local_n, const int my_rank, const int p) {
	int *a = NULL;

	if (my_rank == 0) {
		const int n = p * local_n;
		a = (int *)malloc(n * sizeof(int));
		MPI_Gather(local_a, local_n, MPI_INT, a, local_n, MPI_INT, 0, MPI_COMM_WORLD);
		printf("Tableau :\n");
		for (int i = 0; i < n; i++)
			printf("%d ", a[i]);
		printf("\n\n");
		free(a);
	} else {
		MPI_Gather(local_a, local_n, MPI_INT, a, local_n, MPI_INT, 0, MPI_COMM_WORLD);
	}
}

/**
	@brief Compare deux éléments d'un tableau.
	@param a_p Pointeur vers le premier élément.
	@param b_p Pointeur vers le deuxième élément.
	@return -1 si a est plus petit que b, 0 si a et b sont égaux, 1 si a est plus grand que b.
*/
int compare(const void *a_p, const void *b_p) {
	const int a = *(int *)a_p;
	const int b = *(int *)b_p;

	if (a < b)
		return -1;
	if (a == b)
		return 0;
	return 1;
}

/**
	@brief Trie le tableau local_a pour chaque processus.
	@param local_a Tableau de nombres générés pour chaque processus.
	@param local_n Taille du tableau local_a.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void sort(int local_a[], const int local_n, const int my_rank, const int p) {
	int even_partner;
	int odd_partner;

	int *temp_b = malloc(local_n * sizeof(int));
	int *temp_c = malloc(local_n * sizeof(int));

	if (my_rank % 2 != 0) {
		even_partner = my_rank - 1;
		odd_partner = my_rank + 1;
		if (odd_partner == p)
			odd_partner = -1;
	} else {
		even_partner = my_rank + 1;
		if (even_partner == p)
			even_partner = -1;
		odd_partner = my_rank - 1;
	}

	qsort(local_a, local_n, sizeof(int), compare);

	for (int phase = 0; phase < p; phase++)
		odd_even_iter(local_a, temp_b, temp_c, local_n, phase, even_partner, odd_partner, my_rank, p);

	free(temp_b);
	free(temp_c);
}

/**
	@brief Réalise une itération de l'algo de tri pair impair.
	@param local_a Tableau de nombres trié pour chaque processus.
	@param temp_b Tableau temporaire pour stocker les éléments pairs.
	@param temp_c Tableau temporaire pour stocker les éléments impairs.
	@param local_n Taille du tableau local_a.
	@param phase Phase de l'itération.
	@param even_partner Identifiant du processus partenaire pair.
	@param odd_partner Identifiant du processus partenaire impair.
	@param my_rank Identifiant du processus appelant.
	@param p Nombre total de processus lancés.
*/
void odd_even_iter(int local_a[], int temp_b[], int temp_c[], const int local_n, const int phase, const int even_partner, const int odd_partner, const int my_rank, int p) {
	MPI_Status status;

	if (phase % 2 == 0) {
		if (even_partner >= 0) {
			MPI_Sendrecv(local_a, local_n, MPI_INT, even_partner, 0, temp_b, local_n, MPI_INT, even_partner, 0, MPI_COMM_WORLD, &status);
			if (my_rank % 2 != 0)
				merge_split_high(local_a, temp_b, temp_c, local_n);
			else
				merge_split_low(local_a, temp_b, temp_c, local_n);
		}
	} else {
		if (odd_partner >= 0) {
			MPI_Sendrecv(local_a, local_n, MPI_INT, odd_partner, 0, temp_b, local_n, MPI_INT, odd_partner, 0, MPI_COMM_WORLD, &status);
			if (my_rank % 2 != 0)
				merge_split_low(local_a, temp_b, temp_c, local_n);
			else
				merge_split_high(local_a, temp_b, temp_c, local_n);
		}
	}
}

/**
	@brief Fusionne deux tableaux triés en ordre croissant pour obtenir un seul tableau trié en ordre croissant.
	La fonction merge_split_low fusionne deux tableaux triés en ordre croissant, local_a et temp_b, en un seul tableau trié en ordre croissant temp_c.
	Les éléments de local_a sont comparés avec ceux de temp_b et le plus petit élément est placé dans temp_c.
	Cette opération est répétée jusqu'à ce que tous les éléments soient placés dans temp_c.
	@param local_a Tableau d'entiers contenant les éléments de la première moitié du tableau à fusionner.
	@param temp_b Tableau d'entiers contenant les éléments de la deuxième moitié du tableau à fusionner.
	@param temp_c Tableau d'entiers où les éléments triés sont stockés.
	@param local_n Nombre d'éléments dans le tableau local_a.
*/
void merge_split_low(int local_a[], int temp_b[], int temp_c[], const int local_n) {
	int ai = 0;
	int bi = 0;
	int ci = 0;
	while (ci < local_n) {
		if (local_a[ai] <= temp_b[bi]) {
			temp_c[ci] = local_a[ai];
			ci++;
			ai++;
		} else {
			temp_c[ci] = temp_b[bi];
			ci++;
			bi++;
		}
	}

	memcpy(local_a, temp_c, local_n * sizeof(int));
}

/**
	@brief Fusionne deux tableaux triés en ordre décroissant pour obtenir un seul tableau trié en ordre décroissant.
	La fonction merge_split_high fusionne deux tableaux triés en ordre décroissant, local_a et temp_b, en un seul tableau trié en ordre décroissant temp_c.
	Les éléments de local_a sont comparés avec ceux de temp_b et le plus grand élément est placé dans temp_c.
	Cette opération est répétée jusqu'à ce que tous les éléments soient placés dans temp_c.
	@param local_a Tableau d'entiers contenant les éléments de la première moitié du tableau à fusionner.
	@param temp_b Tableau d'entiers contenant les éléments de la deuxième moitié du tableau à fusionner.
	@param temp_c Tableau d'entiers où les éléments triés sont stockés.
	@param local_n Nombre d'éléments dans le tableau local_a.
*/
void merge_split_high(int local_a[], int temp_b[], int temp_c[], const int local_n) {
	int ai = local_n - 1;
	int bi = local_n - 1;
	int ci = local_n - 1;
	while (ci >= 0) {
		if (local_a[ai] >= temp_b[bi]) {
			temp_c[ci] = local_a[ai];
			ci--;
			ai--;
		} else {
			temp_c[ci] = temp_b[bi];
			ci--;
			bi--;
		}
	}

	memcpy(local_a, temp_c, local_n * sizeof(int));
}
